﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Parallel_1
{
    class Program
    {
        static void temp_task(int a)
        {
            for(int i=0;i<10;i++)
            {
                Console.WriteLine("a is {0} and i is {i}", a, i);
            }
        }

        static void Main(string[] args)
        {
            Task[] set_of_tasks = new Task[5];

            set_of_tasks[0] = new ThreadStart(temp_task());

            Parallel.For(0, 4, i =>
              {
                  set_of_tasks[i].Start();
              });
        }
    }
}
